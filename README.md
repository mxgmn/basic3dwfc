Simple 3-dimensional tiled model for [WaveFunctionCollapse](https://github.com/mxgmn/WaveFunctionCollapse).

![](http://i.imgur.com/zrewvua.png)

Special thanks to kchplr and giawa for helping me with MagicaVoxel file format.
