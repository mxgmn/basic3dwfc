﻿/*
The MIT License(MIT)
Copyright(c) mxgmn 2016.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.
*/

using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.ComponentModel;

static class Stuff
{
	public static int Random(this double[] a, double r)
	{
		double sum = a.Sum();

		if (sum == 0)
		{
			for (int j = 0; j < a.Count(); j++) a[j] = 1;
			sum = a.Sum();
		}

		for (int j = 0; j < a.Count(); j++) a[j] /= sum;

		int i = 0;
		double x = 0;

		while (i < a.Count())
		{
			x += a[i];
			if (r <= x) return i;
			i++;
		}

		return 0;
	}

	public static T Get<T>(this XmlNode node, string attribute, T defaultT = default(T))
	{
		string s = ((XmlElement)node).GetAttribute(attribute);
		var converter = TypeDescriptor.GetConverter(typeof(T));
		return s == "" ? defaultT : (T)converter.ConvertFromInvariantString(s);
	}

	public static Voxel[] ReadVox(string filename) => ReadVox(new BinaryReader(File.Open(filename, FileMode.Open)));

	static Voxel[] ReadVox(BinaryReader stream)
	{
		Voxel[] voxels = null;

		string magic = new string(stream.ReadChars(4));
		int version = stream.ReadInt32();

		while (stream.BaseStream.Position < stream.BaseStream.Length)
		{
			char[] chunkId = stream.ReadChars(4);
			int chunkSize = stream.ReadInt32();
			int childChunks = stream.ReadInt32();
			string chunkName = new string(chunkId);

			if (chunkName == "SIZE")
			{
				int X = stream.ReadInt32();
				int Y = stream.ReadInt32();
				int Z = stream.ReadInt32();
				stream.ReadBytes(chunkSize - 4 * 3);
			}
			else if (chunkName == "XYZI")
			{
				int numVoxels = stream.ReadInt32();
				voxels = new Voxel[numVoxels];
				for (int i = 0; i < voxels.Length; i++) voxels[i] = new Voxel(stream);
			}
		}

		return voxels;
	}

	public static void WriteVox(string filename, int X, int Y, int Z, Voxel[] voxels) => 
		WriteVox(new BinaryWriter(File.Open(filename, FileMode.Create)), X, Y, Z, voxels);

	static void WriteVox(BinaryWriter stream, int X, int Y, int Z, Voxel[] voxels)
	{
		Action<string> writeString = s => { foreach (char c in s) stream.Write(c); };

		writeString("VOX ");
		stream.Write(150);

		writeString("MAIN");
		stream.Write(0);
		stream.Write(40 + voxels.Length * 4);

		writeString("SIZE");
		stream.Write(12);
		stream.Write(0);
		stream.Write(X);
		stream.Write(Y);
		stream.Write(Z);

		writeString("XYZI");
		stream.Write(4 + voxels.Length * 4);
		stream.Write(0);
		stream.Write(voxels.Length);

		foreach (Voxel v in voxels)
		{
			stream.Write(v.x);
			stream.Write(v.y);
			stream.Write(v.z);
			stream.Write(v.color);
		}
	}
}

struct Voxel
{
	public byte x, y, z, color;

	public Voxel(BinaryReader stream)
	{
		x = stream.ReadByte();
		y = stream.ReadByte();
		z = stream.ReadByte();
		color = stream.ReadByte();
	}
}